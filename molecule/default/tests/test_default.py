import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('alarm_annunciators')


def test_alarm_annunciator_running_and_enabled(host):
    service = host.service("alarm-annunciator")
    assert service.is_running
    assert service.is_enabled
